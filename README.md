# Global Game Jam 2022: Duality

## Artwork by ansimuz
https://ansimuz.itch.io/

* Sunnyland: https://ansimuz.itch.io/sunny-land-pixel-game-art
* Sunnyland forest: https://ansimuz.itch.io/sunnyland-forest
* Sunnyland woods: https://ansimuz.itch.io/sunnyland-woods
* Sunnyland enemies: https://ansimuz.itch.io/sunnyland-enemies-extended-pack

