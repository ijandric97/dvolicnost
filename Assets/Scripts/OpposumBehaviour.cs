using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpposumBehaviour : MonoBehaviour
{
    public float walkSpeed = 3;
    public float dashSpeed = 40;
    public float dashRange = 5;
    
    public Transform target;
    
    private Rigidbody2D enemyRigidbody2D;
    private Animator animator;
    
    // Start is called before the first frame update
    void Start()
    {
        enemyRigidbody2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        
        //assign target to player later here
    }
    
    // Update is called once per frame
    void Update()
    {        
        if(Vector3.Distance(target.position, transform.position) > 100) return;
        
        Vector3 direction = target.position - transform.position;
        direction.Normalize();
        transform.localScale = new Vector2(direction.x < 0 ? 1: -1, 1);
        
        float distance = Vector3.Distance(target.position, transform.position);
        
        if(distance < dashRange){
            enemyRigidbody2D.velocity = new Vector2(direction.x * dashSpeed, enemyRigidbody2D.velocity.y);
        } else {
            enemyRigidbody2D.velocity = new Vector2(direction.x * walkSpeed, enemyRigidbody2D.velocity.y);
        }
    }

}
