using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombBehaviour : MonoBehaviour
{
    public int exploadTimer = 2;
    public int damage = 1;
    
    private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        StartCoroutine(Expload());
    }
    
    IEnumerator Expload(){
        yield return new WaitForSeconds(exploadTimer);
        animator.SetBool("end", true);
        
        yield return new WaitForSeconds(0.7f);
        Destroy(gameObject);
    }

}
