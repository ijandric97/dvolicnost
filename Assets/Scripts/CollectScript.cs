using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectScript : MonoBehaviour
{
    private Object thisObject;
    private const string _playerTag = "Player";
    private BackpackUI backpack;

    private void Awake()
    {
        var UI = GameObject.FindWithTag("UI");
        backpack = UI.GetComponent<BackpackUI>();
        thisObject = GetComponent<Object>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag(_playerTag))
        {
            if(gameObject.tag == "gunpowderColectable")
                backpack.gunpowderColected += 1;
            else if(gameObject.tag == "rockColectable")
                backpack.rockColected += 1;
            else if(gameObject.tag == "woodColectable")
                backpack.woodColected += 1;
            Destroy(gameObject);
        }
    }
}
