using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomEnemyBehavious : MonoBehaviour
{
    public int damage;
    // jumping 
    public float jumpForce = 500f;
    public float timeBetweenAttacks;
    
    // for checking when enemy is on ground
    public LayerMask groundLayer;
    public Transform groundCheckPoint;
    public float groundCheckRadius = 5f;
    
    public Transform target;
    
    private Rigidbody2D enemyRigidbody2D;
    private Animator animator;
    
    private bool isTouchingGround;
    private float timer;


    // Start is called before the first frame update
    void Start()
    {
        enemyRigidbody2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        timer = timeBetweenAttacks;
        
        //assign target to player later here
    }
    
    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(target.position, transform.position) > 100) return;
        
        isTouchingGround = Physics2D.OverlapCircle(groundCheckPoint.position, groundCheckRadius, groundLayer);

        // timer for jumps - TODO: generate random number
        if(timer > 0){
            timer -= Time.deltaTime;
        } else {
            jumpAttackMethod();
            timer = timeBetweenAttacks;
        }
        
        // animation variables
        // hardcoded timer for jump animation (try with clip length)
        if(timer < 0.3) animator.SetBool("startJump", true);
        else animator.SetBool("startJump", false);
        
        animator.SetBool("isTouchingGround", isTouchingGround);
        animator.SetFloat("jumpSpeed", enemyRigidbody2D.velocity.y);
    }
    
    void jumpAttackMethod(){
        // direction towards player
        Vector3 direction = target.position - transform.position;
        // when timer ends, jump 
        enemyRigidbody2D.AddForce(new Vector2(direction.x * 20, jumpForce));
    }
}
