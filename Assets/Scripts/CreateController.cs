using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CreateController : MonoBehaviour
{
    public GameObject plankPrefabObject;
    public GameObject bombPrefabObject;
    
    private BackpackUI backpack;
    
    // Start is called before the first frame update
    void Start()
    {
        var UI = GameObject.FindWithTag("UI");
        backpack = UI.GetComponent<BackpackUI>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Keypad1)){
            CreatePlank();
        } else if(Input.GetKeyDown(KeyCode.Keypad2)){
            CreateBomb();
        }
    }
    
    private void CreatePlank(){
        if(backpack.woodColected < 2){
            Debug.Log("You don't have enough wood!");
        }
        
        plankPrefabObject.transform.position = new Vector2(transform.position.x + 1.5f * transform.localScale.x, transform.position.y - 1);
        
        PrefabUtility.InstantiatePrefab(plankPrefabObject);
        
        backpack.woodColected -= 2;
    }
    
    private void CreateBomb(){
        if(backpack.gunpowderColected < 2){
            Debug.Log("You don't have enough gunpowder!");
            return;
        }
        if(backpack.rockColected < 1){
            Debug.Log("You don't have enough rocks!");
            return;
        }
        
        bombPrefabObject.transform.position = new Vector2(transform.position.x + 1, transform.position.y + 1);
        
        PrefabUtility.InstantiatePrefab(bombPrefabObject);
        
        backpack.gunpowderColected -= 2;
        backpack.rockColected -= 1;
    }
}
