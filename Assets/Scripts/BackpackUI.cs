using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BackpackUI : MonoBehaviour
{
    public int gunpowderColected = 0;
    public int woodColected = 0;
    public int rockColected = 0;
    public TextMeshProUGUI gunText;
    public TextMeshProUGUI rockText;
    public TextMeshProUGUI woodText;

    private void LateUpdate()
    {
        gunText.text = gunpowderColected.ToString();
        rockText.text = rockColected.ToString();
        woodText.text = woodColected.ToString();
    }
}
